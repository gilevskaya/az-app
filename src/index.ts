import { c, logger } from '../../az-util/build';
import * as routes from '../../az-routes/build';
// TODO: move ^^ to npm modules
import * as express from 'express';

logger.debug(c.TEST);
logger.info(c.TEST2);

const app = express();
app.disable('x-powered-by');
app.set('json spaces', 2);

// app.use('/api', routes);
const port = process.env.PORT || 3000;
app.listen(port, () => logger.info(`Listening on ${port}`));